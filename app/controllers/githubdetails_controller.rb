class GithubdetailsController < ApplicationController

  def repos
    github = Github.new(user: User::USER_LIST.first)
    @repos_list = github.repos.list.body
  end
end