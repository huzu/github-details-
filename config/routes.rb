Rails.application.routes.draw do
  # get 'sessions/new'

  get '/auth/:provider/callback', to: 'sessions#create'
  get '/auth/failure', to: redirect('/')
  get '/repos', to: 'githubdetails#repos'
  get 'signout', to: 'sessions#destroy', as: 'signout'
  root to: 'sessions#new'
  resources :votes, only: %i[index create]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
